<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShowClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('show_clients', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('identificacion',30);
            $table->string('tipo_id' ,2);
            $table->string('primer_nombre' ,30);
            $table->string('segundo_nombre' ,30)->nullable();
            $table->string('primer_apellido' ,30);
            $table->string('segundo_apellido' ,30)->nullable();
            $table->string('direccion' ,125);
            $table->string('telefono' ,12)->nullable();
            $table->string('email' ,50);
            $table->string('ocupacion' ,100)->nullable();
            $table->string('departamento' ,15)->nullable();
            $table->string('municipio' ,15)->nullable();
            
            $table->timestamps();
        });
    } 

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('show_clients');
    }
}
