<?php

namespace App\Http\Controllers;

use App\show_clients;
use Illuminate\Http\Request;

class ShowClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $dataCliente['clientes'] = show_clients::paginate(10);

        return view('clientes.index',$dataCliente);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    // Recibe los datos de create.blade y luego hace la insercion
    public function store(Request $request)
    {
        
        $dataCliente = request()->all();

        $dataCliente = request()->except('_token');

        show_clients::insert($dataCliente);

        //return response()->json($dataCliente);

        return redirect('clientes/create');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\show_clients  $show_clients
     * @return \Illuminate\Http\Response
     */
    public function show(show_clients $show_clients)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\show_clients  $show_clients
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        
        $cliente = show_clients::findOrFail($id);

        return view('clientes.edit' ,compact('cliente'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\show_clients  $show_clients
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        
        $dataCliente = request()->except(['_token','_method']);

        show_clients::where('id','=',$id)->update($dataCliente);

        return redirect('clientes');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\show_clients  $show_clients
     * @return \Illuminate\Http\Response
     */

    // Eliminar de DB
    public function destroy($id)
    {
        
        show_clients::destroy($id);        

        return redirect('clientes');

    }
}
