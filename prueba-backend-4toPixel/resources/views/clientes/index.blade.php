<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Prueba - 4toPixel</title>
    <!-- Freamework Bulma -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>    	
  </head>

  <body> 

  <section class="section">
    <div class="container">
      <h1 class="title">
      	<center>
        	Lista de clientes
        </center>
      </h1> 

     
<div class="table-container">
	<table class="table">
	  <thead>
	    <tr>
	      <th>Identificación</th>
	      <th>Tipo Identificación</th>
	      <th>Nombres</th>
	      <th>Apellidos</th>
	      <th>Dirección</th>
	      <th>Telefono</th>
	      <th>E-Mail</th>
	      <th>Ocupación</th>
	      <th>Departamento</th>
	      <th>Municipio</th>
	      <th>Acciones</th>
	    </tr>
	  </thead>
	  <tfoot>
	    <tr>
	      <th>Identificación</th>
	      <th>Tipo Identificación</th>
	      <th>Nombres</th>
	      <th>Apellidos</th>
	      <th>Dirección</th>
	      <th>Telefono</th>
	      <th>E-Mail</th>
	      <th>Ocupación</th>
	      <th>Departamento</th>
	      <th>Municipio</th>
	      <th>Acciones</th>
	    </tr>
	  </tfoot>

	  <tbody>
	  	@foreach($clientes as $cliente)
	    <tr>

	      <td>{{$cliente->identificacion}}</td>        
	      <td>{{$cliente->tipo_id}}</td>
	      <td>{{$cliente->primer_nombre.' '.$cliente->segundo_nombre}}</td>
	      <td>{{$cliente->primer_apellido.' '.$cliente->segundo_apellido}}</td>
	      <td>{{$cliente->direccion}}</td>
	      <td>{{$cliente->telefono}}</td>
	      <td>{{$cliente->email}}</td>
	      <td>{{$cliente->ocupacion}}</td>
	      <td>{{$cliente->departamento}}</td>
	      <td>{{$cliente->municipio}}</td>
	      <td>

	      	<a class="button is-info is-rounded" href="{{url('/clientes/'.$cliente->id.'/edit')}}" style="margin-bottom: 3px;">Editar</a>

	      	<form method="post" action="{{url('/clientes/'.$cliente->id)}}">
	      		<!-- dar persimos para usar post a laravel -->
	      		{{csrf_field()}}
	      		<!-- especificar a laravel que se pasa parametro para borrar -->
	      		{{method_field('Delete')}}

	      		<button class="button is-danger is-rounded" type="submit" onclick="return confirm('¿Seguro que desea eliminar este registro?')">Eliminar</button>

	      	</form>
	      </td>      
	    </tr> 
	    @endforeach    
	  </tbody>

	</table>
</div>	



<!-- botones -->
    <div class="container">		
		<div class="level">
			<div class="level-item has-text-centered"></div>				
			<div class="level-item has-text-centered">
				<a class="button is-success is-rounded" href="{{url('/clientes/create')}}">Registrar nuevo cliente</a>	
			</div>
			<div class="level-item has-text-centered"></div>
		</div>	  		  				
	</div>	



    </div>
  </section>
  </body>
</html>



	

