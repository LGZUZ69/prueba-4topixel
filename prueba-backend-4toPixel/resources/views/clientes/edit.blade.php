<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Prueba - 4toPixel</title>
    <!-- Freamework Bulma -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>    	
  </head>

  <body> 

  <section class="section">
    <div class="container">
      <h1 class="title">
      	<center>
        	Editar datos de {{$cliente->primer_nombre.' '.$cliente->primer_apellido}}
        </center>
      </h1> 

     
<form action="{{url('/clientes/'.$cliente->id)}}" method="post">

<!-- llave para poder dejar acceder post laravel -->
{{ csrf_field() }}

<!-- especifiar que vamos hacer un update -->
{{method_field('PATCH')}}

    <!-- Identificcion --> 
	<div class="field is-horizontal">
	  <div class="field-label is-normal">
	    <label class="label">Identificación <i class="has-text-danger">*</i></label>
	  </div>
	  <div class="field-body">
	    <div class="field">	      
	        <input value="{{$cliente->identificacion}}" class="input is-rounded" type="text" placeholder="N° Identificación *" name="identificacion" maxlength="30" required="required" onkeypress="return solonumeros(event)">	        	      
	    </div>

	    <div class="field">	      		 
		    <div class="field is-narrow">		    	
		      <div class="control">		      	
		        <div class="select is-fullwidth is-rounded">
		          <select name="tipo_id" required="required" id="tipoID">
		            <option value="{{$cliente->tipo_id}}" disabled selected>Tipo de identificacion</option>
                        <option value="CC">CC</option>
                        <option value="RC">RC</option>
                        <option value="TI">TI</option>
                        <option value="AS">AS</option>
                        <option value="MS">MS</option>
                        <option value="PA">PA</option>
		          </select>
		        </div>
		      </div>
		    </div>		  
	    </div>

	  </div>
	</div>

<!-- Nombres  -->
	<div class="field is-horizontal">
	  <div class="field-label is-normal">
	    <label class="label">Nombres <i class="has-text-danger">*</i></label>
	  </div>
	  <div class="field-body">
	    <div class="field">	      
	        <input value="{{$cliente->primer_nombre}}" class="input is-rounded" type="text" placeholder="Primer nombre *" name="primer_nombre" maxlength="30" required="required" onkeypress="return sololetras(event)">	        	      
	    </div>

	    <div class="field">	      		 
		    <input value="{{$cliente->segundo_nombre}}" class="input is-rounded" type="text" placeholder="Segundo nombre" maxlength="30" name="segundo_nombre" onkeypress="return sololetras(event)">		  
	    </div>

	  </div>
	</div>

<!-- Apellidos -->
	<div class="field is-horizontal">
	  <div class="field-label is-normal">
	    <label class="label">Apellidos <i class="has-text-danger">*</i></label>
	  </div>
	  <div class="field-body">
	    <div class="field">	      
	        <input value="{{$cliente->primer_apellido}}" class="input is-rounded" type="text" placeholder="Primer apellido *" name="primer_apellido" maxlength="30" required="required" onkeypress="return sololetras(event)">	        	      
	    </div>

	    <div class="field">	      		 
		    <input value="{{$cliente->segundo_apellido}}" class="input is-rounded" type="text" placeholder="Segundo apellido" maxlength="30" name="segundo_apellido" onkeypress="return sololetras(event)">		  
	    </div>

	  </div>
	</div>

<!-- Direccion -->
	<div class="field is-horizontal">
	  <div class="field-label is-normal">
	    <label class="label">Dirección <i class="has-text-danger">*</i></label>
	  </div>
	  <div class="field-body">
	    <div class="field">	      
	        <input value="{{$cliente->direccion}}" class="input is-rounded" type="text" placeholder="Dirección *" maxlength="125" name="direccion" required="required">	        	      
	    </div>

	  </div>
	</div>


<!-- telefono y correo -->
	<div class="field is-horizontal">
	  <div class="field-label is-normal">
	    <label class="label">Contacto <i class="has-text-danger">*</i></label>
	  </div>
	  <div class="field-body">
	    <div class="field">	      
	        <input value="{{$cliente->telefono}}" class="input is-rounded" type="tel" placeholder="Telefono" name="telefono" maxlength="12" onkeypress="return solonumeros(event)">	        	      
	    </div>

	    <div class="field">	      		 
		    <input value="{{$cliente->email}}" class="input is-rounded" type="email" placeholder="E-Mail *" name="email" maxlength="50" required="required" onkeypress="return correo(event)">		  
	    </div>

	  </div>
	</div>


<!-- Ocupacion -->
	<div class="field is-horizontal">
	  <div class="field-label is-normal">
	    <label class="label">Ocupación</label>
	  </div>
	  <div class="field-body">
	    <div class="field">	      
	        <input value="{{$cliente->ocupacion}}" class="input is-rounded" type="text" placeholder="Ocupación" name="ocupacion" maxlength="100" onkeypress="return sololetras(event)">	        	      
	    </div>

	  </div>
	</div>



<!-- Departamento y municipio --> 
	<div class="field is-horizontal">
	  <div class="field-label is-normal">
	    <label class="label">Lugar de residencia</label>
	  </div>
	  <div class="field-body">
	     <div class="field">	      		 
		    <div class="field is-narrow">		    	
		      <div class="control">		      	
		        <div class="select is-fullwidth is-rounded">
		          <select name="departamento">
		            <option disabled selected value="{{$cliente->departamento}}">Seleccione un Departamento:</option>
                        <option value="Bolívar">Bolívar</option>
                        <option value="Atlántico">Atlántico</option>                        
		          </select>
		        </div>
		      </div>
		    </div>		  
	    </div>


	    <div class="field">	      		 
		    <div class="field is-narrow">		    	
		      <div class="control">		      	
		        <div class="select is-fullwidth is-rounded">
		          <select name="municipio">
		            <option value="{{$cliente->municipio}}" disabled selected>Seleccione un municipio</option>
                        <option value="Cartagena">Cartagena</option>
                        <option value="Barranquilla">Barranquilla</option>                        
		          </select>
		        </div>
		      </div>
		    </div>		  
	    </div>

	  </div>
	</div>

	<br>


<!-- botones -->
    <div class="container">
		
			<div class="level">
				<div class="level-item has-text-centered"></div>
				<div class="level-item has-text-centered">
					<button class="button is-info is-rounded" type="submit" onclick="validar_select()">Guardar datos</button>		
				</div>
				<div class="level-item has-text-centered">
					<a class="button is-danger is-rounded" href="{{url('/clientes')}}">Cancelar</a>	
				</div>
				<div class="level-item has-text-centered"></div>
			</div>	  		  		
			
	</div>


</form>	



    </div>
  </section>
  </body>

<!-- validacion de campos con javascript -->
  <script type="text/javascript">
  	
  	function sololetras(a){

	key=a.keyCode || a.which;
	teclado=String.fromCharCode(key).toLowerCase();
	letras=" abcdefghijklmnñopqrstuvwxyz";
	especiales=[8,37,38,164,10,13];
	teclado_especial=false;

	for(var i in especiales){
	  if(key==especiales[i]){
	    teclado_especial=true;break;
	  }
	}

	if(letras.indexOf(teclado)==-1 && !teclado_especial){
	  return false;
	}

	}

	function solonumeros(b){

	key=b.keyCode || b.which;
	teclado=String.fromCharCode(key).toLowerCase();
	letras="1234567890";
	especiales=[8,37,38,164,10,13];
	teclado_especial=false;

	for(var i in especiales){
	  if(key==especiales[i]){
	    teclado_especial=true;break;
	  }
	}

	if(letras.indexOf(teclado)==-1 && !teclado_especial){
	  return false;
	}

	}

	function correo(c){

	key=c.keyCode || c.which;
	teclado=String.fromCharCode(key).toLowerCase();
	letras="._-@1234567890abcdefghijklmnñopqrstuvwxyz";
	especiales=[8,37,38,46,164,10,13];
	teclado_especial=false;

	for(var i in especiales){
	  if(key==especiales[i]){
	    teclado_especial=true;break;
	  }
	}

	if(letras.indexOf(teclado)==-1 && !teclado_especial){
	  return false;
	}

	}

	function validar_select(){
		
		selectVal = document.getElementById("tipoID").value;
		if (selectVal == "") {
			alert("Selecciona un tipo de identificación");
		    selectVal.focus();
		}
		   	
	 
	}

  </script>

</html>



	

